# Conference GO

Conference GO is a conference organizational application designed to help users find conferences to attend and present at.

## Features
* Users can create events
* Users can create presentations
* Users can create conferences
* Users can sign up to attend conferences and give presentations

## Getting Started

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/thedylanminton/conference-go.git

2. **Navigate to Project Directory:**
   ```bash 
   cd conference-go

3. **Build the Application:**
   ```bash
   docker-compose build

4. **Run the Application:**
   ```bash
   docker-compose up
